def nalichie(orders, products_id_on_warehouse):
    for i in orders:
        new_products=[]
        for j in i['products']:
            prod_number=int(j.get('product_id'))
            if prod_number in products_id_on_warehouse:
                new_products.append(j)
        if len(new_products)==0:
            orders.remove(i)
        else:
            new_dict={'products':new_products}
            i.update(new_dict)
    return orders
